import { Injectable } from '@angular/core';

import * as toastr from 'toastr';
import {RequestOptions} from "@angular/http";
//declare var toastr: any;

@Injectable()
export class AppConfiguration {
      constructor(){
          this.setToastrOptions();
          console.log("AppConfiguration ctor");

          if(location.port && (location.port !== "80" || location.port !=="443"))
            //this.urls.baseUrl = "http://localhost/albumviewer/";
            this.urls.baseUrl = "https://albumviewer2swf.west-wind.com/";
            //this.urls.baseUrl = "http://home.west-wind.com/albumviewer/";

      }

      // top level search text
      searchText = "";
      activeTab = "albums";
      isSearchAllowed = true;

      urls = {
        //baseUrl: "http://localhost/albumviewer/",
        baseUrl: "https://albumviewer2swf.west-wind.com/",
        //baseUrl: "http://home.west-wind.com/albumviewer/",
        //baseUrl: "./",
        albums: "api/albums",
        album: "api/album",
        artists: "api/artists",
        artist: "api/artist",
        artistLookup: "api/artistlookup?search=",
        saveArtist: "api/artist",
        login: "api/login",
        logout: "api/logout",
        isAuthenticated: "api/isAuthenticated",
        reloadData: "api/restoreFromBackup",
        url: (name,parm1?,parm2?,parm3?) => {
          var url = this.urls.baseUrl + this.urls[name];
          if (parm1)
            url += "/" + parm1;
          if (parm2)
            url += "/" + parm2;
          if (parm3)
            url += "/" + parm3;

          return url;
        }
      };


      setToastrOptions() {
        toastr.options.closeButton = true;
        toastr.options.positionClass = "toast-bottom-right";
      }

  /**
   * Http Request options to for requests
   * @type {RequestOptions}
   */
  requestOptions =  new RequestOptions({  withCredentials: true });
}

