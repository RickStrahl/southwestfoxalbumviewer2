﻿# AlbumViewer Angular 2.0 Web Connection Sample Application
from Southwest Fox 2016

### What it is
This AlbumViewer application is a heavily modified version of the original MusicStore sample app that ships with Web Connection modified for **Angular2**. It is a client centric Web application using AngularJS 2.0 and Bootstrap that uses a FoxPro backend using JSON to serve the data displayed in the front end application.

The application uses Mobile first design and is designed to run on devices from phones all the way up to desktop class displays and be usable and attractive on all of these device sizes.

**[Live Online Sample Application](https://albumviewer2swf.west-wind.com/)**

For editing operations use the following credentials:

* uid: test@test.com
* pwd: test

This application demonstrates:

**On the server side**  
* Using Web Connection as a JSON Service
* Using extensionless URLs in Web Connection
* Using Business Objects using wwBusiness
 
**On the client side**  
* Using AngularJS to create JavaScript based client application
* Using a modified BootStrap template to create a responsive Mobile First UI
*

### Installation
Once you have the files cloned from BitBucket do the following.

### FoxPro Configuration
Start Visual FoxPro in the `deploy` folder to ensure the paths get set properly.

> #### Modify config.fpw Paths
> Make sure you adjust the paths in config.fpw (or `SetPaths.prg`) to reflect the Web Connection installation folder on your machine. For most of you the base paths should point to:
> ```
> PATH=.\data;.\bus;C:\WCONNECT\FOX\classes;C:\WCONNECT\FOX\;
> ```

The easiest way to do this is to use the generated **Albumviewer - Start FoxPro IDE with Web Connection** shortcut and change the FoxPro and startup path to match the path of your `deploy` folder. I also recommend you set the **Run As Administrator** flag on the shortcut to allow IIS configuration and COM Server compilation support. 

Once paths are set you can start the application:

```foxpro
do AlbumViewerMain
```

The Web Connection server should come up and run and be ready and waiting for requests

### Unzip the data
Go to the `deploy\data` folder and unzip the file contained in the folder into the same folder to expand the AlbumViewer Sample data.

### IIS or IIS Express Configuration
Next, we need to configure IIS to make sure the service API backend is available. You can use IIS Express or IIS but I recommend you use IIS just so the application is always ready to run as IIS Express has to be started explicitly.

If you have IIS installed you can run:

```foxpro
DO AlbumViewer_ServerConfig.prg
```

to configure the IIS virtual called `AlbumViewer`. After this is done you should be able to navigate to:

```
http://localhost/albumviewer
```

to verify the server works and:

```
http://localhost/albumviewer/api/artists
```

to verify the Web Connection server can be contacted.

If you want to use IIS Express and Visual Studio:

* Open Visual Studio
* Open Web Site at `<installFolder>\web`

Then to start IIS Express right click on `index.html` and choose **View in Web Browser**. This will start IIS Express on a custom port. You should then be able to navigate to:

```
http://localhost:3343
http://localhost:3343/api/artists
```
### Angular Client Application Configuration
Open a command window in the `web` folder and issue:

```
npm install
```

This installs the the NPM dependencies required to build and run the client side application. This includes **WebPack**, the **Angular** Libraries, **TypeScript** and a host of other related build and runtime dependencies. This will take a while...

Once installed run:

```
npm start
```

This starts the development Web server and watch program that detects any code changes, which recompiles everything and auto-refreshes the current Web page. You can then navigate to:

```
http://localhost:3000
```

If things are not working, (you just see the loading indicator) check and make sure that the server used for the data is correct. Open up `app\business\appConfiguration.ts` and find the `urls` object and set the appropriate `basePath` property:

```
urls: {
        //baseUrl: "http://localhost/albumviewer/",
        //baseUrl: "http://localhost:3343/",
        //baseUrl: "https://albumviewerswf2.west-wind.com/",
        //baseUrl: "http://home.west-wind.com/albumviewer/",
        baseUrl: "./",
}
```

The value shown would be for IIS, the second for IIS Express. The 3rd is an online URL that runs without FoxPro locally. If you're using IIS Express only you also have to change a small code override to use the path for the development server as it looks to an external server. Change this line to:

 ```javascript
if(location.port && (location.port !== "80" || location.port !=="443"))
    this.urls.baseUrl = "http://localhost:3343/";
```

This ensures you're browsing the HTML on the dev server (on port 3000) and retrieving the data from IIS or IIS Express.